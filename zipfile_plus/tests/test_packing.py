import contextlib
import datetime
import io
import itertools
import numbers
import os
import pathlib
import pprint
import re
import textwrap
import time
import types
import typing
import warnings
try:
    from typing import Literal
except ImportError:
    from typing_extensions import Literal
try:
    from typing import Protocol
except ImportError:
    from typing_extensions import Protocol
try:
    from typing import Self
except ImportError:
    from typing_extensions import Self

import numpy as np
import pytest

from .. import ZipFile, pack, repack


class ApproxTime:
    def __init__(
        self,
        dt: datetime.datetime,
        tol: typing.Optional[numbers.Real] = None,
    ) -> None:
        self.dt = dt
        self.tol = max(0., SECOND_TOLERANCE if tol is None else tol)

    def __repr__(self) -> str:
        return '{}({!r}, tol={:{}})'.format(
            type(self).__name__, self.dt, self.tol, self.fmt,
        )

    def __eq__(
        self: Self, other: typing.Union[Self, datetime.datetime],
    ) -> bool:
        if isinstance(other, ApproxTime):
            tol = max(self.tol, other.tol)
            other = other.dt
        else:
            tol = self.tol
        return abs((self.dt - other).total_seconds()) <= tol

    @classmethod
    def from_seconds(
        cls: typing.Type[Self],
        s: numbers.Real,
        tol: typing.Optional[numbers.Real] = None,
    ) -> Self:
        return cls(datetime.datetime.fromtimestamp(s), tol=tol)

    @classmethod
    def from_nanoseconds(
        cls: typing.Type[Self],
        ns: numbers.Real,
        tol: typing.Optional[numbers.Real] = None,
    ) -> Self:
        return cls.from_seconds(ns * 1E-9, tol=tol)

    fmt = '.3f'

    __hash__ = None


SECOND_TOLERANCE = 2.  # FIXME: timestamp inconsistencies

FILE_HIERACHY: typing.OrderedDict[
    typing.Tuple[str],  # [Directories, ..., ] Filename
    typing.Tuple[
        typing.Union[str, bytes],  # Content
        int,  # Octal mode
        bool,  # Whether to include in the ZIP file
    ]
] = typing.OrderedDict({
    ('foo.txt',): (
        """
    Foo
        """,
        0o644,
        True,
    ),
    ('BARBAR',): (  # Random binary data
        bytes(np.random.default_rng(42).integers(0, 128, 50).tolist()),
        0o600,
        False,
    ),
    ('bar', 'baz'): (
        """
    Lorem ipsum dolor sit amet,
    consectetur adipiscing elit,
    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
    Ut enim ad minim veniam,
    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
    commodo consequat.
    Duis aute irure dolor in reprehenderit in voluptate velit esse
    cillum dolore eu fugiat nulla pariatur.
    Excepteur sint occaecat cupidatat non proident,
    sunt in culpa qui officia deserunt mollit anim id est laborum.
        """,
        0o600,
        True,
    ),
    ('bar', 'foobar', 'main.py'): (
        """
    #!/usr/bin/env python3

    def main() -> None:
        pass

    if __name__ == '__main__':
        main()
        """,
        0o750,
        True,
    ),
    ('bar', 'bar.txt'): (
        """
    This is another test file.
    Foo
    Bar
    Baz
        """,
        0o644,
        False,
    ),
})
STAT_CHECKED_METADATA: typing.Dict[
    str, typing.Union[typing.Callable, None]
] = dict(
    st_mode=oct,
    st_size=None,
    st_mtime_ns=ApproxTime.from_nanoseconds,
    st_birthtime_ns=ApproxTime.from_nanoseconds,
    st_birthtime=ApproxTime.from_seconds,
)
READ_CHUNK_SIZE = 512
for key, (content, *others) in FILE_HIERACHY.items():
    if not isinstance(content, str):
        continue
    FILE_HIERACHY[key] = (textwrap.dedent(content).strip('\n'), *others)
    del key, content, others

T = typing.TypeVar('T')
FileHierachy = typing.Tuple[
    pathlib.PurePath,
    typing.OrderedDict[pathlib.PurePath, typing.Tuple[str, bool]],
]
Key = typing.TypeVar('Key', bound=typing.Hashable)
HashableValue = typing.TypeVar('HashableValue', bound=typing.Hashable)


@pytest.fixture
def file_hierachy(tmp_path: pathlib.Path) -> FileHierachy:
    """
    Type
    ----
    >>> tuple[  # noqa: F821 # doctest: +SKIP
    ...     abs/path/to/root/dir,
    ...     dict[
    ...         abs/path/to/created/file,
    ...         (string_content, file_to_be_zipped)
    ...     ],
    ... ]

    File structure
    --------------
    <tmp_path>/
     +- root/
         +- foo.txt
         +- BARBAR
         +- bar/
             +- baz
             +- foobar/
             |   +- main.py
             +- bar.txt
    """
    root = tmp_path / 'root'
    root.mkdir()
    created_files: typing.OrderedDict[
        pathlib.PurePath, typing.Tuple[str, bool]
    ] = typing.OrderedDict()
    for path, (content, permissions, in_zip, *_) in FILE_HIERACHY.items():
        *path_chunks, fname = path
        file_path = root.joinpath(*path_chunks)
        file_path.mkdir(parents=True, exist_ok=True)
        file_path /= fname
        # Note: something seems to be wrong with the timestamps of the
        # files staged by the fixture;
        # try to enforce uniformity by checking for the existence of the
        # files
        if file_path.exists():
            msg = '{}: not tempering with existing {}'.format(
                tmp_path, file_path.relative_to(tmp_path),
            )
            warnings.warn(msg)
        else:  # Stage content and permissions
            with file_path.open(
                mode=('w' if isinstance(content, str) else 'wb'),
            ) as fobj:
                fobj.write(content)
            file_path.chmod(permissions)
        created_files[file_path] = (content, in_zip)
    return root, created_files


@pytest.fixture
def verbose(pytestconfig: pytest.Config) -> bool:
    return pytestconfig.getoption('verbose', default=False)


@pytest.fixture
def archive(
    tmp_path: pathlib.Path,
    file_hierachy: FileHierachy,
    verbose: bool,
) -> str:
    """
    File structure
    --------------
    <tmp_path>/
     +- root/
     |   +- foo.txt
     |   +- BARBAR
     |   +- bar/
     |       +- baz
     |       +- foobar/
     |       |   +- main.py
     |       +- bar.txt
     +- created_archive.zip
         +- foo.txt
         +- bar/baz
         +- bar/foobar/main.py
    """
    root, created_files = file_hierachy
    root_str = str(root)
    if not root_str.endswith('/'):
        root_str += '/'
    time.sleep(1)
    return pack(
        str(tmp_path / 'created_archive.zip'),
        {
            # Pass the paths as a mapping, or `pack()` will
            # automatically take just the basenames of the files
            str(path): str(path.relative_to(root))
            for path, (_, in_zip) in created_files.items()
            if in_zip
        },
        verbose=verbose,
    )


def group(
    kvps: typing.Mapping[Key, HashableValue]
) -> typing.Dict[HashableValue, typing.Set[Key]]:
    groups: typing.Dict[HashableValue, typing.Set[Key]] = {}
    for key, value in kvps.items():
        groups.setdefault(value, set()).add(key)
    return groups


def pairwise(
    iterable: typing.Iterable[T],
) -> typing.Generator[typing.Tuple[T, T], None, None]:
    """
    Backport of `itertools.pairwise()` from Python 3.10.

    Example
    -------
    >>> ','.join(''.join(pair) for pair in pairwise('ABCDEFG'))
    'AB,BC,CD,DE,EF,FG'
    """
    iterator = iter(iterable)
    a = next(iterator, None)
    for b in iterator:
        yield a, b
        a = b


def compare_archive_entries(
    zf1: typing.Union[str, ZipFile],
    zf2: typing.Union[str, ZipFile],
    arc_name_1: str,
    arc_name_2: typing.Optional[str] = None,
    exclude_attrs: typing.Optional[typing.Collection[str]] = None,
) -> None:
    compared_attributes = [
        'date_time',
        'comment',
        'extra',
        'flag_bits',
        'internal_attr',
        'external_attr', 
        'CRC',
        'file_size',
    ]
    compared_attributes: typing.Collection[str] = (
        set(compared_attributes) - set(exclude_attrs or ())
    )
    with contextlib.ExitStack() as stack:
        zf1 = stack.enter_context(
            ZipFile(zf1)
            if isinstance(zf1, str) else
            contextlib.nullcontext(zf1)
        )
        zf2 = stack.enter_context(
            ZipFile(zf2)
            if isinstance(zf2, str) else
            contextlib.nullcontext(zf2)
        )
        zinfo1 = zf1.getinfo(arc_name_1)
        zinfo2 = zf2.getinfo(arc_name_2 or arc_name_1)
        # Compare metadata
        for attr in compared_attributes:
            try:
                value1 = getattr(zinfo1, attr)
            except AttributeError:
                continue
            try:
                value2 = getattr(zinfo2, attr)
            except AttributeError:
                continue
            assert value1 == value2
        # Compare contents
        assert zf1.read(zinfo1) == zf2.read(zinfo2)


def compare_files(
    *fnames: typing.Union[str, pathlib.PurePath],
    stat_attrs: typing.Mapping[
        str, typing.Union[typing.Callable, None]
    ] = types.MappingProxyType({
        attr: converter for attr, converter in STAT_CHECKED_METADATA.items()
        if attr != 'st_mode'  # Note: ZIP doesn't keep permissions
    }),
    read_chunk_size: int = READ_CHUNK_SIZE,
    binary: typing.Optional[bool] = None,
    content: typing.Optional[typing.Union[str, bytes]] = None,
    compare_raw: bool = False,
) -> None:
    stat_results = [os.stat(file) for file in fnames]
    # Check metadata
    for attr, converter in stat_attrs.items():
        if not all(hasattr(stat_result, attr) for stat_result in stat_results):
            warnings.warn(
                'One or more `os.stat()` results don\'t have the attribute '
                f'`.{attr}`'
            )
            continue
        raw_values = [
            getattr(stat_result, attr) for stat_result in stat_results
        ]
        values = [
            converter(value) if converter else value
            for value in raw_values
        ]
        compared_values = raw_values if compare_raw else values
        try:
            if len(set(compared_values)) <= 1:
                continue
        except TypeError:  # Unhashable
            for x, y in pairwise(compared_values):
                if x != y:
                    break
            else:  # All comparisons equal
                continue
        if compare_raw:
            comparisons = {
                '{!r}: {!r}': value
                for fname, raw_value, value in zip(fnames, raw_values, values)
            }
        else:
            comparisons = dict(zip(fnames, values))
        try:
            repr_comp = repr(group(comparisons))
        except TypeError:  # Unhashable
            repr_comp = repr(comparisons)
            repr_comp_structure = f'{{filename: {attr}}}'
        else:
            repr_comp_structure = f'{{{attr}: list[filename]}}'
        raise AssertionError(
            '`.{}` differs among the `os.stat()` results of the files ({}):\n{}'
            .format(attr, repr_comp_structure, repr_comp)
        )
    # Check contents
    with contextlib.ExitStack() as stack:
        if content is not None and binary is None:
            binary = isinstance(content, bytes)
        if binary:
            mode, IO = 'rb', io.BytesIO
        else:
            mode, IO = 'r', io.StringIO
        fobjs = [
            stack.enter_context(open(fname, mode=mode)) for fname in fnames
        ]
        if content is not None:
            buffer_io = stack.enter_context(IO(content))
            buffer_io.seek(0)
            fobjs.insert(0, buffer_io)
            fnames = ['<reference content>', *fnames]
        for i in itertools.count():
            chunks = [fobj.read(read_chunk_size) for fobj in fobjs]
            if not any(chunks):  # EOF for all files
                break
            if len(set(chunks)) <= 1:
                continue
            chunk_values = dict(zip(fnames, chunks))
            try:
                repr_chunk = pprint.pformat(group(chunk_values))
            except TypeError:  # Unhashable
                repr_chunk = pprint.pformat(chunk_values)
                repr_chunk_structure = '{filename: chunk}'
            else:
                repr_chunk_structure = '{chunk: list[filename]}'
            raise AssertionError(
                'Chunk #{} (each of size {}) differs among the files ({}):\n{}'
                .format(i, read_chunk_size, repr_chunk_structure, repr_chunk)
            )


def test_pack_archive(
    tmp_path: pathlib.Path,
    file_hierachy: FileHierachy,
    archive: str,
) -> None:
    """
    Create an archive with `~.pack()` and test its contents.
    """
    _, created_files = file_hierachy
    extract_to = tmp_path / 'extract_target'
    with ZipFile(archive) as zfobj:
        zfobj.extractall(extract_to)
    for created_path, (path_chunks, (content, _, in_zip, *_)) in zip(
        created_files, FILE_HIERACHY.items(),
    ):
        if not in_zip:
            continue
        extracted_path = extract_to.joinpath(*path_chunks)
        compare_files(created_path, extracted_path, content=content)


@pytest.mark.parametrize(
    'dest, function, args, kwargs',
    [
        (  # Rename some files
            None,
            'pack',
            None,
            dict(
                rename={
                    re.escape('bar' + os.path.sep):
                    re.escape('baz' + os.path.sep)
                },
                regex=True,
            ),
        ),
        (  # Delete a file
            None, 'repack', None, dict(delete='foo.txt'),
        ),
        (  # Add a file
            'new_archive.zip', 'repack', {'BARBAR': 'blahblah.bin'}, None,
        ),
    ],
)
def test_repacking_preserves_metadata(
    tmp_path: pathlib.Path,
    file_hierachy: FileHierachy,
    archive: str,
    dest: typing.Union[str, None],
    function: Literal['pack', 'repack'],
    args: typing.Union[typing.Sequence[str], typing.Mapping[str, str], None],
    kwargs: typing.Union[typing.Mapping[str, typing.Any], None],
    verbose: bool,
) -> None:
    """
    Check if [re-]packing keeps the file birth timestamp and the
    permissions of the archive intact.
    """
    def get_stat_results(
        fname: typing.Union[str, pathlib.PurePath],
    ) -> typing.Dict[str, typing.Any]:
        results = {}
        stat_result = os.stat(fname)
        for attr in checked_metadata:
            try:
                value = getattr(stat_result, attr)
            except AttributeError:
                warnings.warn(
                    f'`os.stat()` result doesn\'t have the attribute `.{attr}`'
                )
                continue
            converter = STAT_CHECKED_METADATA.get(attr)
            if converter:
                value = converter(value)
            results[attr] = value
        return results

    def joinpath(path: typing.Union[str, pathlib.PurePath]) -> str:
        return str(root.joinpath(path))

    checked_metadata = 'st_mode', 'st_birthtime', 'st_birthtime_ns'
    root, created_files = file_hierachy
    old_stat_results = get_stat_results(archive)
    # Process dest, *args, **kwargs
    dest = joinpath(dest) if dest else None
    args = args or ()
    if isinstance(args, typing.Mapping):
        args = {
            joinpath(src): target for src, target in args.items()
        },
        args_is_mapping = True
    else:
        args = [joinpath(fname) for fname in args]
        args_is_mapping = False
    kwargs = kwargs or {}
    kwargs.setdefault('verbose', verbose)
    time.sleep(1)
    # (Re-)Pack and check the metadata
    if function == 'pack':
        archive = pack(dest or archive, *args, **{'base': archive, **kwargs})
    else:
        archive = repack(
            archive, 
            **{
                'dest': dest,
                'add': (args[0] if args_is_mapping else args),
                **kwargs,
            },
        )
    new_stat_results = get_stat_results(archive)
    assert old_stat_results == new_stat_results


@pytest.mark.parametrize('compare_entries', (True, False))
@pytest.mark.parametrize(
    'args, kwargs, expected_content',
    [
        (  # Rename a file
            None,
            dict(
                rename={
                    os.path.join('bar', 'baz'): os.path.join('bar', 'bazz'),
                },
            ),
            {
                'foo.txt': ('foo.txt',),
                os.path.join('bar', 'bazz'): ('bar', 'baz'),
                os.path.join('bar', 'foobar', 'main.py'): (
                    'bar', 'foobar', 'main.py',
                ),
            },
        ),
        (  # Replace a file
            {os.path.join('bar', 'bar.txt'): 'foo.txt'},
            dict(delete='foo.txt'),
            {
                'foo.txt': ('bar', 'bar.txt'),
                os.path.join('bar', 'baz'): ('bar', 'baz'),
                os.path.join('bar', 'foobar', 'main.py'): (
                    'bar', 'foobar', 'main.py',
                ),
            },
        ),
    ],
)
def test_repack_archive_contents(
    tmp_path: pathlib.Path,
    file_hierachy: FileHierachy,
    archive: str,
    args: typing.Union[typing.Sequence[str], typing.Mapping[str, str], None],
    kwargs: typing.Union[typing.Mapping[str, typing.Any], None],
    compare_entries: bool,
    expected_content: typing.Mapping[str, Literal[tuple(FILE_HIERACHY)]],
    verbose: bool,
) -> None:
    """
    Update an archive with `~.repack()` and test its contents.
    """
    def joinpath(*path: typing.Union[str, pathlib.PurePath]) -> str:
        return str(root.joinpath(*path))

    root, created_files = file_hierachy
    # Process *args, **kwargs
    args = args or ()
    if isinstance(args, typing.Mapping):
        args = {
            joinpath(src): target for src, target in args.items()
        },
        args_is_mapping = True
    else:
        args = [joinpath(fname) for fname in args]
        args_is_mapping = False
    kwargs = kwargs or {}
    kwargs.setdefault('verbose', verbose)
    time.sleep(1)
    # Repack and check content
    if compare_entries:
        dest = str(tmp_path / 'new_archive.zip')
        repack(
            archive,
            **{
                'dest': dest,
                'add': (args[0] if args_is_mapping else args),
                **kwargs,
            },
        )
        with contextlib.ExitStack() as stack:
            zf1 = stack.enter_context(ZipFile(archive))
            zf2 = stack.enter_context(ZipFile(dest))
            assert sorted(zf2.namelist()) == sorted(expected_content)
            for fname2, fh_key in expected_content.items():
                _, _, in_zip, *_ = FILE_HIERACHY[fh_key]
                if in_zip:
                    fname1 = os.path.join(*fh_key)
                    compare_archive_entries(zf1, zf2, fname1, fname2)
                else:
                    extract_to = tmp_path / 'extract_target'
                    extract_to.mkdir(exist_ok=True)
                    stored_path = joinpath(*fh_key)
                    extracted_path = zf2.extract(fname2, str(extract_to))
                    compare_files(extracted_path, stored_path)
    else:  # Same ZIP file edited
        repack(
            archive,
            **{'add': (args[0] if args_is_mapping else args), **kwargs},
        )
        extract_to = tmp_path / 'extract_target'
        with ZipFile(archive) as zfobj:
            assert sorted(zfobj.namelist()) == sorted(expected_content)
            zfobj.extractall(str(extract_to))
        for arc_name, fh_key in expected_content.items():
            extracted_path = str(extract_to / arc_name)
            content, *_ = FILE_HIERACHY[fh_key]
            compare_files(extracted_path, stat_attrs={}, content=content)
