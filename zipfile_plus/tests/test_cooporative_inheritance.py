"""
Test whether `~.ZipFileBase` enables cooperative inheritance.
"""
import typing

import pytest  # noqa: F401

from .. import ZipFileBase


def dummy_property(name: str) -> property:
    """
    Property object which falls through to item access on the instance
    `.__dict__`;
    only defined here so that we can explicitly declare that an
    attribute exists at the class level.
    """
    def getter(self):
        return vars(self)[name]

    def setter(self, value):
        vars(self)[name] = value

    def deleter(self):
        del vars(self)[name]

    return property(getter, setter, deleter)


class WithAttributeA(ZipFileBase):
    def __init__(self, *args, a: typing.Optional[int] = None, **kwargs) -> None:
        super().__init__(*args, a=a, **kwargs)
        self.a = a

    a: typing.Optional[int] = dummy_property('a')


class WithAttributeA2(ZipFileBase):
    def __init__(self, *args, a: typing.Optional[int] = None, **kwargs) -> None:
        super().__init__(*args, a=a, **kwargs)
        self.a2 = a if a is None else (a * self.a2_multiplier)

    a2: typing.Optional[float] = dummy_property('a2')
    a2_multiplier: float = .25


class WithAttributeB(ZipFileBase):
    def __init__(self, *args, b: typing.Optional[int] = None, **kwargs) -> None:
        super().__init__(*args, b=b, **kwargs)
        self.b = b

    b: typing.Optional[int] = dummy_property('b')


def test_takes_arbitrary_kwargs(tmp_path) -> None:
    """
    Test that a `~.ZipFileBase` subclass takes arbitrary keyword
    arguments.
    """
    with WithAttributeA(tmp_path / 'foo.zip', mode='w', a=1, b=2, c=3) as zfobj:
        assert zfobj.a == 1
        assert not hasattr(zfobj, 'b')
        assert not hasattr(zfobj, 'c')
        assert zfobj.namelist() == []


def test_shunting_of_kwargs(tmp_path) -> None:
    """
    Test that a class inheriting from multiple `~.ZipFileBase`
    subclasses shunts the taken keyword arguments to the correct
    base-class `.__init__()` implementations.
    """
    class WithAttributesAB(WithAttributeA, WithAttributeB):
        pass

    with WithAttributesAB(tmp_path / 'foo.zip', mode='w', a=1, b=2) as zfobj:
        assert zfobj.a == 1
        assert zfobj.b == 2
        assert zfobj.namelist() == []


def test_overlapping_kwargs(tmp_path) -> None:
    """
    Test that a class inheriting from multiple `~.ZipFileBase`
    subclasses provides the taken keyword arguments to the correct
    base-class `.__init__()` implementations, even when multiple base
    classes share the same keyword arguments.
    """
    class WithAllAttributes(WithAttributeA, WithAttributeA2, WithAttributeB):
        pass

    with WithAllAttributes(tmp_path / 'foo.zip', mode='w', a=1, b=2) as zfobj:
        assert zfobj.a == 1
        assert zfobj.a2 == zfobj.a * zfobj.a2_multiplier
        assert zfobj.b == 2
        assert zfobj.namelist() == []
