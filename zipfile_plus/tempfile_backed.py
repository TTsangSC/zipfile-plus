"""
Text-mode and/or real-file-backed IO on `zipfile.ZipFile`.

Notes
-----
Python 3.9 introduced upgrades to `zipfile.Path.open()` which already
allows binary-/text-mode toggling.
"""
import io
import os
import shutil
import tempfile
import types
import typing
import warnings
import zipfile
try:  # python3.8+
    from typing import Literal
except ImportError:
    from typing_extensions import Literal
try:  # python3.11+
    from typing import Self
except ImportError:
    from typing_extensions import Self

from .base import ZipFileBase

__all__ = ('FileExistsWarning', 'WithTempFiles', 'ZipFile')

ArchiveMember = typing.Union[str, zipfile.ZipInfo]
_Exception = typing.TypeVar('_Exception', bound=Exception)

FILE_MODES = 'r', 'w', 'x', 'a'
OPEN_MODES = 'r', 'w'
WRITABLE_MODES = 'w', 'x', 'a'


class FileExistsWarning(UserWarning, FileExistsError):
    """
    Warning class to signal that files exists when they shouldn't.
    """


class WithTempFiles(ZipFileBase):
    """
    Extension to `zipfile.ZipFile` with the option to `.open()` archive
    members for reading using real (OS-level) file handles for various
    applications.

    Example
    -------
    >>> import contextlib
    >>> import io
    >>> import os
    >>> import tempfile
    >>> import zipfile
    >>> import numpy as np

    Numpy raises `io.UnsupportedOperation` since `f` has no OS-level
    handle:

    >>> with contextlib.ExitStack() as stack:
    ...     tmpdir = stack.enter_context(tempfile.TemporaryDirectory())
    ...     zf = stack.enter_context(
    ...         zipfile.ZipFile(os.path.join(tmpdir, 'data.zip'), 'a'),
    ...     )
    ...     with zf.open('data.txt', 'w') as f:
    ...         _ = f.write(b'1 2 3 4')
    ...     try:  # This read fails
    ...         with zf.open('data.txt', 'r') as f:
    ...             array = np.fromfile(f, sep=' ')
    ...         assert array.tolist() == [1, 2, 3, 4]
    ...     except io.UnsupportedOperation:
    ...         pass
    ...     else:
    ...         raise AssertionError('numpy is supposed to choke here')

    But this works since a temporary file is written to then read from:

    >>> with contextlib.ExitStack() as stack:
    ...     tmpdir = stack.enter_context(tempfile.TemporaryDirectory())
    ...     zf = stack.enter_context(
    ...         WithTempFiles(os.path.join(tmpdir, 'data.zip'), 'a'),
    ...     )
    ...     with zf.open('data.txt', 'w') as f:
    ...         _ = f.write(b'1 2 3 4')
    ...     with zf.open(
    ...         'data.txt', 'r', tempfile=True,
    ...     ) as f:  # This read passes
    ...         array = np.fromfile(f, sep=' ')
    ...     assert array.tolist() == [1, 2, 3, 4]
    """
    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self._tempdir = tempfile.mkdtemp()

    def open(
        self,
        name: ArchiveMember,
        mode: Literal[OPEN_MODES] = 'r',
        pwd: typing.Optional[bytes] = None,
        *,
        force_zip64: bool = False,
        tempfile: bool = False,
        text: bool = False,
        **kwargs
    ) -> typing.IO[typing.Union[bytes, str]]:
        """
        Parameters
        ----------
        tempfile
            True
                Back the file object to be written to/read from with a
                physical file so that it has an OS-level handle
            False
                Default parent-class behavior
        text
            True
                Communicated with the file object in text instead of
                bytes mode
            False
                Default parent-class behavior
        *args, **kwargs
            See parent class;
            anonymous `kwargs` are passed to `io.TextIOWrapper` if
            `text` is true

        Notes
        -----
        - For tempfile mode:
          - Since tempfiles are written to and read from, it is good
            practice to either use contexts or `.close()` to manage
            their lifetimes.
            If the `ZipFile` object is closed when any spawned temporary
            file still exists, a `FileExistsWarning` is issued.
          - No writing to the archive actually occurs until the file
            object is closed via exiting its context, object deletion,
            or explicit calls to `.close()`.

        - For non-tempfile mode (default):
          - If `text = True`, the returned file object is wrapped by
            `io.TextIOWrapper`
        """
        if not tempfile:
            fobj = super().open(name, mode, pwd, force_zip64=force_zip64)
            return io.TextIOWrapper(fobj, **kwargs) if text else fobj
        elif mode == 'r':
            return TempfileReadWrapper(self, name, pwd, text=text)
        elif mode == 'w':
            if self.mode == 'r':
                raise ValueError(
                    f'mode = {mode!r}, .mode = {self.mode!r}: '
                    '.write() requires any of the following modes: '
                    f'{WRITABLE_MODES!r}'
                )
            return TempfileWriteWrapper(self, name, text=text)
        else:
            raise ValueError(
                f'mode = {mode!r}: '
                f'open() requires any of the following modes: {OPEN_MODES!r}'
            )

    def close(self) -> None:
        super().close()
        try:
            if not os.path.isdir(self._tempdir):
                pass
            elif os.listdir(self._tempdir):
                msg = (
                    'Temporary file(s) persist(s) and will be deleted; '
                    'remember to properly `.close()` all file objects spawned '
                    'by `.open()` by using them as context managers or by '
                    'explicitly calling `fobj.close()` on them.'
                )
                warnings.warn(msg, FileExistsWarning)
                shutil.rmtree(self._tempdir)
            else:
                os.rmdir(self._tempdir)
        except Exception:
            pass


ZipFile = WithTempFiles


class TempfileWrapperBase:
    """
    Base wrapper around `tempfile.NamedTemporaryFile()`, to be returned
    by`WithTempFiles.open()`.
    Subclasses are to define their own `.close()` method and read/write
    methods where appropriate.
    """
    def __init__(
        self,
        zfobj: WithTempFiles,
        mode: Literal[FILE_MODES],
        name: ArchiveMember,
    ) -> None:
        self._zipfileobject = zfobj
        self._fileobject = tempfile.NamedTemporaryFile(
            mode, dir=self._zipfileobject._tempdir,
        )
        self._arcname = name

    def __getattr__(self, attr: str) -> typing.Any:
        """
        Defer undefined member access to the wrapped file object.
        """
        return getattr(self._fileobject, attr)

    def __repr__(self) -> str:
        return (
            '{type}(name={name!r}, zfobj={zfobj!r}, wrapped_fobj={fobj!r})'
            .format(
                type=type(self).__name__,
                name=self._arcname,
                zfobj=self._zipfileobject,
                fobj=self._fileobject,
            )
        )

    def __enter__(self: Self) -> Self:
        self._fileobject = self._fileobject.__enter__()
        return self

    def __exit__(
        self,
        exception: typing.Union[typing.Type[_Exception], None],
        value: typing.Union[_Exception, None],
        traceback: typing.Union[types.TracebackType, None],
    ) -> None:
        self.close()

    def __del__(self) -> None:
        self.close()

    def close(self) -> None:
        """
        Delete the temporary file.
        """
        try:
            self._fileobject.close()
        except FileNotFoundError:
            pass

    @property
    def _zipfileobject(self) -> WithTempFiles:
        return self._zfobj

    @_zipfileobject.setter
    def _zipfileobject(self, zfobj: WithTempFiles) -> None:
        if isinstance(zfobj, WithTempFiles):
            self._zfobj = zfobj
        else:
            raise ValueError(
                f'zfobj = {zfobj!r}: not a {WithTempFiles!r} object'
            )


class TempfileReadWrapper(TempfileWrapperBase):
    """
    Wrapper around a readable tempfile to be returned by
    `WithTempFiles.open(..., mode='r', tempfile=True)`
    """
    def __init__(
        self,
        zfobj: WithTempFiles,
        name: ArchiveMember,
        pwd: typing.Optional[bytes] = None,
        text: bool = False,
    ) -> None:
        super().__init__(zfobj, 'r' if text else 'rb', name)
        with open(self.name, mode='wb') as fobj:
            fobj.write(self._zipfileobject.read(self._arcname, pwd))
        self._iterator = iter(self._fileobject)

    # Note: tempfiles only have `.__iter__()` but not `.__next__()`, so
    # special care is needed
    def __iter__(self: Self) -> Self:
        return self

    def __next__(self) -> typing.AnyStr:
        return next(self._iterator)


class TempfileWriteWrapper(TempfileWrapperBase):
    """
    Wrapper around a writable tempfile to be returned by
    `WithTempFiles.open(..., mode='w', tempfile=True)`
    """
    def __init__(
        self,
        zfobj: WithTempFiles,
        name: ArchiveMember,
        text: bool = False,
    ) -> None:
        super().__init__(zfobj, 'w' if text else 'wb', name)
        if isinstance(self._arcname, str):
            pass
        elif isinstance(self._arcname, zipfile.ZipInfo):
            self._arcname = self._arcname.filename
        else:
            msg = (
                f'name = {self._arcname!r}: '
                'expected a filename or a `zipfile.ZipInfo` object'
            )
            raise TypeError(msg)

    def flush(self) -> None:
        """
        Write the content in the tempfile to the archive.
        """
        self._fileobject.flush()
        self._zipfileobject.write(self.name, self._arcname)

    def close(self) -> None:
        try:
            self.flush()
        except Exception:
            pass
        finally:
            super().close()
