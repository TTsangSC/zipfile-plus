"""
Extensions for the functionalities of the `zipfile` stdlib module.
"""
import operator
import textwrap
import types
import typing
import warnings
import zipfile
try:
    from typing import Literal
except ImportError:
    from typing_extensions import Literal

from .archive_packing import pack, repack
from .base import ZipFileBase
from .preserve_timestamps import WithPreservedTimestamps
from .tempfile_backed import WithTempFiles

__all__ = (
    # Data
    'ZIPFILE_EXTENSION_IMPLEMENTED_IN',
    # Classes
    'ZipFileBase',
    'ZipFile',
    # Functions
    'pack',
    'repack',
    'get_zipfile_subclass',
    '__getattr__',
    '__dir__',
)

ZIPFILE_EXTENSION_IMPLEMENTED_IN: typing.Dict[str, typing.Type[ZipFileBase]] = {
    'text-mode': WithTempFiles,
    'tempfile-backed': WithTempFiles,
    'preserve-timestamps': WithPreservedTimestamps,
}
_SUBCLASSES: typing.Dict[  # Base classes -> auto subclass
    typing.Tuple[typing.Type[ZipFileBase]], typing.Type[ZipFileBase]
] = {
}
_PACKAGE_ = (lambda: None).__module__
if _PACKAGE_.endswith('.__init__'):
    _PACKAGE_ = _PACKAGE_.rpartition('.')[0]


def get_zipfile_subclass(
    *features: Literal[tuple(ZIPFILE_EXTENSION_IMPLEMENTED_IN)]
) -> typing.Type[ZipFileBase]:
    """
    Return
    ------
    An appropriate `zipfile.ZipFile` (sub-)class with the requisite
    features.

    Example
    -------
    >>> from _PACKAGE_ import (
    ...     get_zipfile_subclass,
    ...     ZIPFILE_EXTENSION_IMPLEMENTED_IN,
    ... )
    >>>
    >>> feature = 'text-mode'
    >>> ZF1 = get_zipfile_subclass(feature)
    >>> assert ZF1 is ZIPFILE_EXTENSION_IMPLEMENTED_IN[feature]
    >>> features = 'text-mode', 'tempfile-backed'
    >>> ZF2 = get_zipfile_subclass(*features)
    >>> assert all(
    ...     issubclass(ZF2, ZIPFILE_EXTENSION_IMPLEMENTED_IN[feature])
    ...     for feature in features
    ... )
    """
    def strip_doc(string: str) -> str:
        return textwrap.dedent(string).strip('\n')

    doc_template = """
    Automatically-created `zipfile.ZipFile` subclass with these
    features:
    {}
    """
    indent = ' ' * 2
    subclasses: typing.Tuple[typing.Type[ZipFileBase]] = tuple(sorted(
        {ZIPFILE_EXTENSION_IMPLEMENTED_IN[feature] for feature in features},
        key=operator.attrgetter('__qualname__'),
    ))
    if not subclasses:  # No features requested
        return ZipFileBase
    if len(subclasses) == 1:  # Only features from one class needed
        return subclasses[0]
    if subclasses in _SUBCLASSES:  # Requisite subclass already exists
        return _SUBCLASSES[subclasses]
    # Build a new docstring for the composite class
    doc_chunks: typing.List[str] = []
    doc_chunks.append(
        strip_doc(doc_template)
        .format(textwrap.fill(
            ', '.join(features),
            width=68,
            initial_indent=indent,
            subsequent_indent=indent,
        ))
    )
    for subcls in subclasses:
        subcls_header = 'Docs ({}.{})'.format(
            subcls.__module__, subcls.__name__,
        )
        subcls_header += '\n{}'.format('-' * len(subcls_header))
        doc_chunks.append(
            '{}\n{}'.format(subcls_header, strip_doc(subcls.__doc__))
        )
    docstring = '\n\n'.join(doc_chunks)
    return _SUBCLASSES.setdefault(
        subclasses, type('ZipFile', subclasses, dict(__doc__=docstring)),
    )


def __getattr__(attr: str) -> typing.Any:
    """
    Retrieve the requested name from `zipfile`.

    Example
    -------
    >>> import zipfile
    >>>
    >>> import _PACKAGE_
    >>>
    >>> assert _PACKAGE_.is_zipfile is zipfile.is_zipfile
    """
    value = getattr(zipfile, attr)
    return globals().setdefault(attr, value)


def __dir__() -> typing.List[str]:
    """
    Return the suitable names that should be accessible at the module
    level;
    also set `.__all__`.

    Return
    ------
    List of names

    Example
    -------
    Note that `zipfile.ZipInfo` has not been explicitly included in
    `.__all__`;
    its inclusion happens due to this function.

    >>> import _PACKAGE_
    >>>
    >>> assert 'ZipInfo' in dir(_PACKAGE_)  # __dir__() triggered
    >>> assert 'ZipInfo' in _PACKAGE_.__all__
    """
    if getattr(__dir__, '_imported_names', None):
        # Names already imported from `zipfile`
        return __all__

    T = typing.TypeVar('T', bound=typing.Hashable)
    Name = typing.TypeVar('Name', bound=str)
    Names = typing.List[Name]
    categories = 'class', 'submodule', 'function', 'data object'
    Category = Literal[categories]

    def unique(
        values: typing.Iterable[T],
        encountered: typing.Optional[typing.Set[T]] = None,
    ) -> typing.Generator[T, None, None]:
        encountered: typing.Set[T] = encountered or set()
        for value in values:
            if value in encountered:
                continue
            encountered.add(value)
            yield value

    module_name = __dir__.__module__
    if module_name.endswith('.__init__'):  # Remove trailing '.__init__'
        module_name = module_name[:-len('.__init__')]
    local_name_lists: typing.Dict[Category, Names] = {}
    zipfile_name_lists: typing.Dict[Category, Names] = {}
    local_name_categorizations: typing.Dict[Name, Category] = {}
    zipfile_name_categorizations: typing.Dict[Name, Category] = {}
    for namespace_name, namespace, names, name_lists, name_categorizations in (
        (
            module_name, globals(), __all__,
            local_name_lists, local_name_categorizations,
        ),
        (
            'zipfile', vars(zipfile), zipfile.__all__,
            zipfile_name_lists, zipfile_name_categorizations,
        ),
    ):  # Sort names under the namespaces into categories
        for category in categories:
            name_lists.setdefault(category, [])
        for name in unique(names):
            if name not in namespace:
                msg = (
                    '{0}: name `{1}` in `{0}.__all__` but not found in the '
                    'module namespace'
                ).format(namespace_name, name)
                warnings.warn(msg)
                continue
            value = namespace[name]
            category: Category = (
                'class'
                if isinstance(value, type) else
                'submodule'
                if isinstance(value, types.ModuleType) else
                'function'
                if callable(value) else
                'data object'
            )
            name_lists[category].append(name)
            name_categorizations[name] = category
    # Sanity check: if a local name shadows a `zipfile` name, said name
    # is at least of the same category between the two
    for name in (
        set(local_name_categorizations) & set(zipfile_name_categorizations)
    ):
        local_cat, zipfile_cat = (
            categorizations[name]
            for categorizations in (
                local_name_categorizations, zipfile_name_categorizations,
            )
        )
        if local_cat != zipfile_cat:
            msg = (
                'name `{}` categorized as a {} in `zipfile` but redefined as '
                'a {} in `{}`'
            ).format(name, zipfile_cat, local_cat, module_name)
            raise AssertionError(msg)
    # Removed shadowed `zipfile` names
    for category, name_list in zipfile_name_lists.items():
        zipfile_name_lists[category] = list(
            unique(name_list, set(local_name_categorizations))
        )
    # Update `.__all__`
    globals()['__all__'] = tuple(
        name
        for category in categories
        for name_lists in (local_name_lists, zipfile_name_lists)
        for name in name_lists[category]
    )
    # Update flag to prevent re-resolution of names
    __dir__._imported_names = True
    return __all__


ZipFile = get_zipfile_subclass('tempfile-backed', 'preserve-timestamps')

__version__ = '1.1.1'

# Fix doctests
for func in get_zipfile_subclass, __getattr__, __dir__:
    func.__doc__ = func.__doc__.replace('_PACKAGE_', _PACKAGE_)
    del func
