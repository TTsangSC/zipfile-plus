"""
Utilities for archive (re-)packing.
"""
import datetime
import inspect
import os
import re
import shutil
import sys
import typing
import zipfile
from contextlib import ExitStack
from tempfile import TemporaryDirectory

from .preserve_timestamps import ZipFile

__all__ = (
    'pack', 'repack',
)

FilePath = typing.TypeVar('FilePath', bound=str)
ExistingPath = typing.TypeVar('ExistingPath', bound=FilePath)
RegexPath = typing.TypeVar('RegexPath', bound=str)


def _is_name(name: typing.Any) -> bool:
    if not isinstance(name, str):
        return False
    return bool(name)


def _copy_zipinfo_data(zipinfo: zipfile.ZipInfo) -> zipfile.ZipInfo:
    new_inst = zipfile.ZipInfo(zipinfo.filename, zipinfo.date_time)
    for attr in zipfile.ZipInfo.__slots__:
        try:
            value = getattr(zipinfo, attr)
        except AttributeError:
            continue
        if callable(value):
            continue
        try:
            setattr(new_inst, attr, value)
        except AttributeError:
            continue
    return new_inst


def _get_consolidated_add(
    add: typing.Mapping[ExistingPath, FilePath],
) -> typing.Generator[typing.Tuple[ExistingPath, FilePath], None, None]:
    """
    Consolidate what files are to be added and where to add them.

    Parameters
    ----------
    add
        Mapping from paths to names;
        the paths must exist and can be either be files or directories

    Yield
    -----
    Pairs of `(path, archive_member_name)`;
    directories are recursively descended into for files, and each such
    file makes for an individual pair.
    """
    for path, mapped_path in add.items():
        if os.path.isfile(path):
            yield path, mapped_path
            continue
        assert os.path.isdir(path), path
        for subpath, _, filenames in os.walk(path):
            assert subpath.startswith(path), f'{subpath}, {path}'
            mapped_subpath = mapped_path + subpath[len(path):]
            yield subpath, mapped_subpath
            for fname in filenames:
                pair = tuple(
                    os.path.join(p, fname) for p in (subpath, mapped_subpath)
                )
                if not all(_is_name(n) for n in pair):
                    raise ValueError(
                        f'{path!r} -> {mapped_subpath!r}: '
                        f'invalid submapping {pair[0]!r} -> {pair[1]!r}'
                    )
                yield pair


def _echo(*args, **kwargs) -> None:
    print(*args, **dict(dict(file=sys.stderr, flush=True), **kwargs))


try:
    _echo.__signature__ = inspect.signature(print)
except ValueError:
    # Can't find signature for some builtins in some versions
    pass


def _no_op(*args, **kwargs) -> None:
    pass


def _summarize(
    src: ExistingPath,
    dest: FilePath,
    new_names: typing.Dict[FilePath, typing.Tuple[FilePath, bool]],
    delete: typing.Collection[FilePath],
    header: bool = True,
) -> typing.Generator[str, None, None]:
    """
    Yield lines summarizing what has been done.

    Parameters
    ----------
    src, dest
        String archive names
    new_names
        Mapping from string names in the new archive to a tuple
        `str_original_name, bool_file_was_from_src`
    delete
        String names dropped from `src`
    header
        Whether to include a header line

    Yield
    -----
    String lines of summary
    """
    def give_total(items: typing.Collection, action: str) -> str:
        n = len(items)
        annotation = annotations.get(action)
        return (
            ' '
            .join(('{}',) * (4 if annotation else 3))
            .format(n, 'item' if n == 1 else 'items', action, annotation)
        )

    def repr_item(item: str) -> str:
        if not isinstance(item, tuple):  # Single file
            return repr(item)
        if item[0] == item[1]:  # No renaming
            return repr(item[0])
        return '{!r} <- {!r}'.format(*item)  # Renamed file

    actions: typing.Dict[
        str,
        typing.Union[
            typing.List[str], typing.List[typing.Tuple[str, str]],
        ]
    ] = {'excluded': sorted(delete)} if delete else {}
    annotations: typing.Dict[str, str] = dict(
        renamed='(current name <- original name)',
        added='(if renamed: current name <- original name)',
    )
    for new, (old, is_from_archive) in new_names.items():
        if is_from_archive and new == old:
            actions.setdefault('included', []).append(new)
        elif is_from_archive:
            actions.setdefault('renamed', []).append((new, old))
        else:
            actions.setdefault('added', []).append((new, old))
    # Output
    if _are_distinct_paths(src, dest):
        action_alias = {}
        if header:
            yield f'{dest!r}: created from {src!r}'
    else:
        action_alias = dict(included='untouched', excluded='deleted')
        if header:
            yield f'{src!r}: edited'
    for action in 'included', 'renamed', 'excluded', 'added':
        items = actions.get(action)
        if not items:
            continue
        yield give_total(items, action_alias.get(action, action))
        yield from (repr_item(item) for item in items)


def _are_distinct_paths(path: FilePath, other: FilePath) -> bool:
    """
    Check whether the paths are resolved to distinct locations.
    """
    def _get_canon(p) -> typing.Tuple[str, str]:
        # Existing, nonexisting
        if os.path.exists(p):
            return os.path.realpath(p), ''
        preceding, last = os.path.split(p)
        if not preceding:
            # Entire path is nonexistent and relative to pwd
            return os.path.realpath(preceding), last
        existing, not_existing = _get_canon(preceding)
        if not existing:  # Entire path is nonexistent
            return os.path.realpath(existing), not_existing
        return existing, os.path.join(not_existing, last)

    return _get_canon(path) != _get_canon(other)


def pack(
    archive: FilePath,
    *add_files: typing.Union[
        ExistingPath, typing.Mapping[ExistingPath, FilePath],
    ],
    base: typing.Optional[ExistingPath] = None,
    rename: typing.Optional[
        typing.Union[
            typing.Mapping[FilePath, FilePath],
            typing.Mapping[RegexPath, RegexPath],
        ]
    ] = None,
    delete: typing.Optional[
        typing.Union[
            FilePath, typing.Collection[FilePath],
            RegexPath, typing.Collection[RegexPath],
        ]
    ] = None,
    regex: typing.Optional[bool] = None,
    regex_rename: typing.Optional[bool] = None,
    regex_delete: typing.Optional[bool] = None,
    force: typing.Optional[bool] = None,
    force_rename: typing.Optional[bool] = None,
    force_add: typing.Optional[bool] = None,
    force_creation: typing.Optional[bool] = None,
    verbose: bool = False,
    **kwargs
) -> FilePath:
    """
    Create a ZIP archive, optionally based on an existing one.

    Parameters
    ----------
    archive
        Path to write the archive to
    *add_files
        path(s) to add to the archive, to be added by basenames;
        alternatively, if a single mapping, treat as being from paths to
        archive member names
    base
        Optional path to an existing archive on which to base `archive`
        on
    rename
        Optional mapping from `base` to new `archive` member names;
        only keys actually existing in `base` are used, the remainder
        are ignored;
        can also be a mapping from regex patterns to substitutions,
        where matching files are renamed by substitution (if `regex` is
        true)
    delete
        Optional member name or names of `base` to drop from `archive`;
        if any of said names does not exist in `base`, it is ignored;
        can also be regex patterns, where matching files are dropped (if
        `regex` is true)
    regex
        Default for the `regex_{rename|delete}` arguments;
        can be overridden by the individual specific arguments
    regex_rename, regex_delete
        Whether to use regex matching for `rename` (resp. `delete`)
    force
        Default for the `force_{rename|add|creation}` arguments;
        can be overridden by the individual specific arguments
    force_rename
        Whether to allow renaming which replaces a filename to be
        otherwise carried over from `base`
    force_add
        Whether to allow addition which replaces a filename to be
        otherwise carried over from `base`
    force_creation
        Whether to allow file creation/overwriting when `archive`
        already exists
    verbose
        Whether to give a summary of the actions done on `archive`
    **kwargs
        Constructor arguments for the `zipfile.ZipFile` object written
        to;
        default `{'compression': zipfile.ZIP_DEFLATED}`

    Return
    ------
    String path to the created/edited archive

    Errors
    ------
    - If `regex[_{rename|delete}]` is true and a `base` member matches
      items in both `rename` and `delete`.

    - If `regex[_rename]` is true and a `base` member matches more than
      one patterns in `rename`.

    - Any condition for which `repack()` raises an error.

    Side effects
    ------------
    `archive` created;
    a temporary directory is used but deleted before the function
    returns or raises.

    Guarantees
    ----------
    `archive` is only created/overwritten when all the actions
    succeeded.

    Notes
    -----
    - In regex mode, matches and replacements are local (made on parts
      of the string) unless otherwise specified (via "^" and "$");
      following the default of `re.sub()`, all matches are replaced.
    - `rename` is ONLY applied to `base` and NOT to the `add_files`;
      use the single-mapping form of `add_files` for renaming new files
      as you add them.

    See also
    --------
    `repack()` (argument names: `archive` -> `dest`, `add_files` ->
    `add`, `base` -> `src`)
    """
    # Checks and massaging: add_files
    if len(add_files) == 1:
        add, = add_files
    else:
        add = add_files
    # regex_rename, regex_delete
    if regex_rename is None:
        regex_rename = regex
    regex_rename = base is not None and rename is not None and regex_rename
    if regex_delete is None:
        regex_delete = regex
    regex_delete = base is not None and delete is not None and regex_delete
    # rename
    if regex_rename:
        if not (
            isinstance(rename, typing.Mapping) and
            all(isinstance(item, str) for kvp in rename.items() for item in kvp)
        ):
            raise TypeError(
                f'rename = {rename!r}: '
                'expected string-to-string mapping if provided'
            )
        rename, rename_passed = {}, rename
        with ZipFile(base, mode='r') as zf_base:
            members = zf_base.namelist()
        for member in members:  # Resolve replacements
            rename_matches = {
                pattern: replacement
                for pattern, replacement in rename_passed.items()
                if re.findall(pattern, member)
            }
            if not rename_matches:
                continue
            if len(rename_matches) == 1:
                (pattern, replacement),  = rename_matches.items()
                rename[member] = re.sub(pattern, replacement, member)
                continue
            raise RuntimeError(
                f'base = {base!r}, rename = {rename_passed!r}: '
                f'archive member {member!r} matches more than one renaming '
                f'pattern: {list(rename_matches)!r}'
            )
    # delete
    if regex_delete:
        if isinstance(delete, str):
            delete = delete,
        if not (
            isinstance(delete, typing.Collection) and
            all(_is_name(d) for d in delete)
        ):
            raise TypeError(
                f'delete = {delete!r}: expected non-empty string(s) if provided'
            )
        with ZipFile(base, mode='r') as zf_base:
            delete = [  # Resolve matches
                member for member in zf_base.namelist()
                if any(re.findall(pattern, member) for pattern in delete)
            ]
    # verbose
    echo = _echo if verbose else _no_op
    # Early exit: no base, just "re-pack" an empty archive
    if base is None:
        with TemporaryDirectory() as tmp_dir:
            base = os.path.join(tmp_dir, 'dummy.zip')
            with ZipFile(base, mode='w'):  # Create the dummy
                pass
            dest, new_names, _ = _repack(
                base,
                dest=archive,
                add=add,
                force=force,
                force_creation=force_creation,
                verbose=False,
                **kwargs,
            )
        echo(f'{archive!r}: created')
        for line in _summarize(base, archive, new_names, set(), header=False):
            echo(line)
        return dest
    # Do stuff
    return repack(
        base,
        dest=archive,
        rename=rename,
        delete=delete,
        add=add,
        force=force,
        force_rename=force_rename,
        force_add=force_add,
        force_creation=force_creation,
        verbose=verbose,
        **kwargs,
    )


def repack(*args, **kwargs) -> ExistingPath:
    """
    Repack a ZIP archive, renaming and/or deleting files from it, and/or
    adding more files.

    Parameters
    ----------
    src
        Path to an existing archive
    dest
        Optional path to write the repackaged archive to;
        default is to overwrite `src`
    rename
        Optional mapping from old to new archive member names;
        only keys actually existing in `src` are used, the remainder are
        ignored
    delete
        Optional archive member name or names to drop;
        if any of said names does not exist, it is ignored
    add
        Optional path(s) to add to the archive;
        if path(s), they are added by basenames;
        if a mapping, treat as being from paths to archive member names
    force
        Default for the `force_{rename|add|creation}` arguments;
        can be overridden by the individual specific arguments
    force_rename
        Whether to force archive member renaming
    force_add
        Whether to force file addition to the archive
    force_creation
        Whether to force file creation (when `dest` is distinct from
        `src`)
    verbose
        Whether to give a summary of the actions done on the archive
    **kwargs
        Constructor arguments for the `zipfile.ZipFile` object written
        to;
        default `{'compression': zipfile.ZIP_DEFLATED}`

    Return
    ------
    String path to the created/edited archive

    Errors
    ------
    - If any (item) of `rename`, `delete`, and `add` is the empty
      string.

    - If members/files are `rename`-d and/or `add`-ed to the same name.

    - If the same member is both to be `rename`-d and `delete`-d.

    - If `rename` (resp. `add`) puts a member at an occupied name and
      `force_rename` (resp. `force_add`) is not resolved to true.

    - If a new archive is to be created (i.e. `dest` distinct from
      `src`), but the path already exists and `force_creation` is false.

    - If any of the addition, renaming, and carrying-over causes files
      to end up with the same archive member names, where the
      corresponding `force[_{add,rename}]` arguments are not true.

    Side effects
    ------------
    Archive `dest` created or `src` updated;
    a temporary directory is used but deleted before the function
    returns or raises.

    Guarantees
    ----------
    `src` is only changed when all the repackaging actions succeeded.
    """
    dest, *_ = _repack(*args, **kwargs)
    return dest


def _repack(
    src: ExistingPath,
    *,
    dest: typing.Optional[FilePath] = None,
    rename: typing.Optional[typing.Mapping[FilePath, FilePath]] = None,
    delete: typing.Optional[
        typing.Union[FilePath, typing.Collection[FilePath]]
    ] = None,
    add: typing.Optional[
        typing.Union[
            ExistingPath,
            typing.Collection[ExistingPath],
            typing.Mapping[ExistingPath, FilePath],
        ]
    ] = None,
    force: typing.Optional[bool] = None,
    force_rename: typing.Optional[bool] = None,
    force_add: typing.Optional[bool] = None,
    force_creation: typing.Optional[bool] = None,
    verbose: bool = False,
    **kwargs
) -> typing.Tuple[
    FilePath,
    typing.Dict[FilePath, typing.Tuple[FilePath, bool]],
    typing.Set[FilePath],
]:
    """
    See `repack()`.

    Return
    ------
    Tuple

    >>> (  # noqa: F821 # doctest: +SKIP
    ...     path/to/archive/created_or_changed,
    ...     dict[new_archive_name, tuple[old_name, file_is_from_src]],
    ...     set[deleted_name],
    ... )
    """
    # Checks and massaging: src
    if not (_is_name(src) and zipfile.is_zipfile(src)):
        raise TypeError(
            f'src = {src!r}: '
            'expected string path to an existing ZIP archive'
        )
    # dest
    if dest is None:
        dest = src
    if not isinstance(dest, str):
        raise TypeError(f'dest = {dest!r}: expected string path')
    dname, _ = os.path.split(dest)
    if dname and not os.path.isdir(dname):
        raise NotADirectoryError(f'dest = {dest!r}: {dname!r} not a directory')
    # rename
    if rename is None:
        rename = {}
    if not (
        isinstance(rename, typing.Mapping) and
        all(_is_name(i) for item in rename.items() for i in item)
    ):
        raise TypeError(
            f'rename = {rename!r}: expected name-to-name mapping if provided'
        )
    if len(set(rename.values())) < len(rename):
        raise ValueError(f'rename = {rename!r}: expected unique values')
    # delete
    if delete is None:
        delete = ()
    if isinstance(delete, str):
        delete = delete,
    if not (
        isinstance(delete, typing.Collection) and
        all(_is_name(d) for d in delete)
    ):
        raise TypeError(
            f'delete = {delete!r}: expected name or names if provided'
        )
    delete = set(delete)
    rename_delete_clashes = set(rename) & delete
    if rename_delete_clashes:
        raise ValueError(
            f'rename = {rename!r}, delete = {delete!r}: '
            f'these keys are found in both: {rename_delete_clashes!r}'
        )
    # add
    if add is None:
        add = {}
    if isinstance(add, str):
        add = (add,)
    if (
        isinstance(add, typing.Collection) and
        all(
            _is_name(a) and (os.path.isfile(a) or os.path.isdir(a)) for a in add
        )
    ):
        if not isinstance(add, typing.Mapping):
            add = {a: os.path.basename(a) for a in add}
    else:
        raise TypeError(
            f'add = {add!r}: '
            'expected string path or paths to existing files and directories, '
            'or mapping from such existing paths to names'
        )
    add = dict(  # Look for dirs and descend into them
        _get_consolidated_add(add)
    )
    if len(set(add.values())) < len(add):
        raise ValueError(f'add = {add!r}: expected unique values')
    inv_rename, inv_add = (
        {v: k for k, v in mapping.items()} for mapping in (rename, add)
    )
    rename_add_clashes = set(inv_rename) & set(inv_add)
    if rename_add_clashes:
        raise ValueError(
            f'rename = {rename!r}, add = {add!r}: '
            f'these values are found in both: {rename_add_clashes!r}'
        )
    # force*
    if force_rename is None:
        force_rename = force
    if force_add is None:
        force_add = force
    if force_creation is None:
        force_creation = force
    # verbose
    echo = _echo if verbose else _no_op
    # kwargs
    kwargs = dict(dict(compression=zipfile.ZIP_DEFLATED), **kwargs)
    # Bookkeeping for files: deletion
    with ZipFile(src, mode='r') as zf:
        members = set(zf.namelist())
        delete &= members
        members -= delete
    # Note: members overwritten by add should also be marked as deleted
    # for bookkeeping purposes
    overwritten_members = members & set(add.values())
    if overwritten_members:
        delete |= overwritten_members
        if not force_add:
            raise ValueError(
                f'add = {add!r}: these new already exists in {src!r}: '
                f'{sorted(overwritten_members)!r}'
            )
    # Carrying-over
    new_names: typing.Dict[
        # New name -> (old member name, True) or (added path, False)
        FilePath, typing.Tuple[FilePath, bool]
    ] = {member: (member, True) for member in (members - set(rename))}
    # Renaming and addition
    rename = {k: v for k, v in rename.items() if k in members}
    for source_name_map, arg_name, is_from_archive, force_member_write in (
        (rename, 'rename', True, force_rename),
        (add, 'add', False, force_add),
    ):
        for source, name in source_name_map.items():
            if name in new_names and not force_member_write:
                other_orig_name, other_is_from_src = new_names[name]
                if other_is_from_src:
                    repr_other = (
                        f'member {other_orig_name!r} from archive {src!r}'
                    )
                else:
                    repr_other = f'file {other_orig_name!r}'
                raise ValueError(
                    f'{arg_name}[{source!r}] = {name!r}: '
                    f'clashes with (new) name of {repr_other}'
                )
            new_names[name] = (source, is_from_archive)

    # Early exits
    if not (rename or delete or add):  # No change
        if _are_distinct_paths(src, dest):
            if os.path.exists(dest) and not force_creation:
                raise FileExistsError(f'dest = {dest!r}: already exists')
            shutil.copy(src, dest)
            echo(f'{dest!r}: copied from {src!r}')
        else:
            echo(f'{src!r}: no-op')
        return dest, new_names, delete
    # Create tmpdir and new archive
    with ExitStack() as outer_stack:
        tmp_dir_zip = outer_stack.enter_context(TemporaryDirectory())
        tmp = os.path.join(tmp_dir_zip, os.path.basename(src))
        if rename or delete:
            # Entries renamed/deleted -> create from ground up
            mode = 'w'
            tmp_dir_ext = outer_stack.enter_context(TemporaryDirectory())
            assert tmp_dir_zip != tmp_dir_ext

            def get_src(stack: ExitStack) -> ZipFile:
                return stack.enter_context(ZipFile(src, mode='r'))

            def move_item_through_buffer(
                zf_old: ZipFile,
                zf_new: ZipFile,
                fname_old: str,
                fname_new: str,
            ) -> None:
                zipinfo = _copy_zipinfo_data(zf_old.getinfo(fname_old))
                zipinfo.filename = fname_new
                zf_new.writestr(zipinfo, zf_old.read(fname_old))

            def move_item_through_disk(
                zf_old: ZipFile,
                zf_new: ZipFile,
                fname_old: str,
                fname_new: str,
            ) -> None:
                fname_temp = zf_old.extract(fname_old, path=tmp_dir_ext)
                zf_new.write(fname_temp, fname_new)

            move_item = move_item_through_buffer

        else:  # Only additions -> append to copy
            shutil.copy(src, tmp)
            mode = 'a'
            get_src = move_item = _no_op
        with ExitStack() as inner_stack:
            zf_old = get_src(inner_stack)
            zf_new = inner_stack.enter_context(
                ZipFile(tmp, mode=mode, **kwargs)
            )
            for new_name, (old_name, is_from_archive) in new_names.items():
                if is_from_archive:  # Files from the old archive
                    move_item(zf_old, zf_new, old_name, new_name)
                else:  # Added files
                    zf_new.write(old_name, new_name)
        # Move to destination when done
        if (
            os.path.exists(dest) and
            _are_distinct_paths(src, dest) and
            not force_creation
        ):
            raise FileExistsError(f'dest = {dest!r}: already exists')
        # Copy attributes, but touch the atime and mtime
        now = datetime.datetime.now().timestamp()
        shutil.copystat(src, tmp)
        os.utime(tmp, (now, now))
        shutil.move(tmp, dest)

    # Print summary
    for line in _summarize(src, dest, new_names, delete):
        echo(line)
    return dest, new_names, delete


repack.__signature__ = (
    inspect.signature(_repack).replace(return_annotation=ExistingPath)
)
