"""
Wrapper around `zipfile.ZipFile` to facilitate cooperative inheritance.
"""
import functools
import inspect
import itertools
import textwrap
import typing
import zipfile

__all__ = 'ZipFileBase',

_get_varkwarg = functools.partial(
    inspect.Parameter, kind=inspect.Parameter.VAR_KEYWORD,
)


def _add_varkwarg_param(
    sig: inspect.Signature,
    annotation: typing.Any = inspect.Parameter.empty,
) -> inspect.Signature:
    """
    Add a varkwarg parameter if there isn't already one;
    the name for the added parameter is taken from the list

    >>> [  # doctest: +SKIP
    ...     'kwargs', 'kw', 'keywords', 'kwds', 'kwargs_', 'kwargs__',
    ...     ...
    ... ]

    If `annotation` is provided, also set it.
    """
    # Check `sig`
    try:
        *other_params, last_param = sig.parameters.values()
    except ValueError:  # No args
        other_params, takes_vakwargs = [], False
    else:
        takes_vakwargs = last_param.kind is inspect.Parameter.VAR_KEYWORD
        if not takes_vakwargs:
            other_params = [*other_params, last_param]
    # Append a varkwarg parameter
    if not takes_vakwargs:
        propose_varkwarg_name = functools.partial(
            next,
            iter(itertools.chain(
                ('kwargs', 'kw', 'keywords', 'kwds'),
                ('kwargs' + ('_' * n) for n in itertools.count(1)),
            )),
        )
        param_name = propose_varkwarg_name()
        while param_name in sig.parameters:
            param_name = propose_varkwarg_name()
        last_param = _get_varkwarg(param_name)
    # Update the annotation
    if annotation is not inspect.Parameter.empty:
        last_param = last_param.replace(annotation=annotation)
    return sig.replace(parameters=[*other_params, last_param])


class ZipFileBase(zipfile.ZipFile):
    """
    Inspect the signature of `super().__init__` and only feed it
    compatible arguments.

    Notes
    -----
    Only deal with the keyword-only arguments;
    subclasses should thus define its initializer like:

    >>> class ZipFileSubclass(...):  # noqa # doctest: +SKIP
    ...     def __init__(
    ...         self,
    ...         *args,
    ...         my_kwarg_1 = ...,
    ...         my_kwarg_2 = ...,
    ...         ...,
    ...         **kwargs,
    ...     ) -> None:
    ...         super().__init__(
    ...             *args,
    ...             my_kwarg_1=my_kwarg_1, my_kwarg_2=my_kwarg_2,
    ...             **kwargs,
    ...         )
    ...         # Do whatever with `my_kwarg_1`, ... here

    In general, subclasses should count on `super().__init__()` to
    discard the incompatible keyword arguments and should NOT do that on
    their own.
    This is to enable inheritance from multiple `ZipFileBase`
    subclasses, where more than one thereof may depend on any single
    keyword-only argument.

    Base-class documentation
    ------------------------
    """
    def __init__(self, *args, **kwargs) -> None:
        super_init = super().__init__
        sig = inspect.signature(super_init)
        try:
            ba = sig.bind(*args, **kwargs)
        except TypeError:
            pass
        else:
            return super_init(*ba.args, **ba.kwargs)
        *_, last_param = sig.parameters.values()
        if last_param.kind is inspect.Parameter.VAR_KEYWORD:
            # If we're here the `TypeError` wasn't due to stray kwargs,
            # so just bind it again and re-raise the error
            sig.bind(*args, **kwargs)
        # Append a varkwarg parameter, then drop everything bound to it
        dummy_sig = _add_varkwarg_param(sig)
        *_, varkwarg_name = dummy_sig.parameters
        ba = dummy_sig.bind(*args, **kwargs)
        del ba.arguments[varkwarg_name]
        super_init(*ba.args, **ba.kwargs)

    __init__.__signature__ = _add_varkwarg_param(
        inspect.signature(zipfile.ZipFile.__init__)
    )


ZipFileBase.__doc__ = '\n'.join(
    textwrap.dedent(cls.__doc__).strip('\n')
    for cls in (ZipFileBase, zipfile.ZipFile)
)
