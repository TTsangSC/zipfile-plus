"""
Preserve timestamps on extraction.
"""
import datetime
import inspect
import os
import typing
import zipfile

from .base import ZipFileBase

__all__ = 'WithPreservedTimestamps', 'ZipFile',


class WithPreservedTimestamps(ZipFileBase):
    """
    `ZipFile` subclass which, as far as possible, sets the ([a]ccess and
    [m]odified) timestamps of the extracted files to be consistent with
    the archive member upon extraction.

    Example
    -------
    >>> import functools
    >>> import os
    >>> import tempfile
    >>> from contextlib import ExitStack
    >>> from datetime import datetime
    >>>
    >>> def compare_ns_timestamps(*timestamps: int) -> bool:
    ...     time_tuples = [
    ...         datetime.fromtimestamp(ts * 1E-9).timetuple()[:6]
    ...         for ts in timestamps
    ...     ]
    ...     return len(set(time_tuples)) <= 1
    ...
    >>> with ExitStack() as stack:
    ...     tmpdir = stack.enter_context(tempfile.TemporaryDirectory())
    ...     joinpath = functools.partial(os.path.join, tmpdir)
    ...     fname = 'foo.txt'
    ...     fobj = stack.enter_context(open(joinpath(fname), mode='w'))
    ...     print('FOO', file=fobj)
    ...     zfobj = stack.enter_context(
    ...         WithPreservedTimestamps(joinpath('bar.zip'), mode='w'),
    ...     )
    ...     zfobj.write(fobj.name, fname)
    ...     # Test `extract()`
    ...     extract_dir = joinpath('foobar')
    ...     os.mkdir(extract_dir)
    ...     extracted_file = zfobj.extract(fname, extract_dir)
    ...     timestamps = [
    ...         os.stat(path).st_mtime_ns
    ...         for path in (fobj.name, extracted_file)
    ...     ]
    ...     assert compare_ns_timestamps(*timestamps), (
    ...         '`.extract()` gives wrong timestamp'
    ...     )
    ...     # Test `extractall()`
    ...     extract_dir = joinpath('baz')
    ...     os.mkdir(extract_dir)
    ...     zfobj.extractall(extract_dir)
    ...     timestamps = [
    ...         os.stat(path).st_mtime_ns
    ...         for path in (
    ...             fobj.name,
    ...             os.path.join(extract_dir, fname),
    ...         )
    ...     ]
    ...     assert compare_ns_timestamps(*timestamps), (
    ...         '`.extractall()` gives wrong timestamps'
    ...     )

    Notes
    -----
    Since it is the time tuple `year, month, day, hour, minute, second`
    that is stored in `ZipInfo.date_time`, resolution of the timestamp
    only goes that fine.
    """
    def extract(
        self,
        member: typing.Union[str, zipfile.ZipInfo],
        *args, **kwargs
    ):
        """
        Extract the file and edit its timestamps to be consistent with
        that of the `ZipInfo`'s.
        """
        if not isinstance(member, zipfile.ZipInfo):
            member = self.getinfo(member)
        # atime = datetime.datetime.now().timestamp()
        atime = mtime = datetime.datetime(*member.date_time).timestamp()
        extracted_path = super().extract(member, *args, **kwargs)
        os.utime(extracted_path, (atime, mtime))
        return extracted_path

    def extractall(
        self, path=None, members=None, *args, **kwargs
    ):
        """
        Extract the files and edit their timestamps to be consistent
        with those of the `ZipInfo`s'.
        """
        if members is None:
            members = self.infolist()
        for member in members:
            self.extract(member, path, *args, **kwargs)

    for method in extract, extractall:
        method.__signature__ = inspect.signature(
            getattr(ZipFileBase, method.__name__),
        )
        del method


ZipFile = WithPreservedTimestamps
